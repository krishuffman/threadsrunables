package runnableexample;

public class ThreadsDemo {

    public static void main(String [] args) {

        System.out.println("Starting first Thread...");
        Thread thread1 = new NumberGame(3);
        thread1.start();
        try {
            thread1.join();
        } catch (InterruptedException e) {
            System.out.println("Thread interrupted.");
        }
        
        System.out.println("Starting second Thread...");
        Thread thread2 = new NumberGame(7);
        thread2.start();
        try {
            thread1.join();
        } catch (InterruptedException e) {
            System.out.println("Thread interrupted.");

        }
    }
}
package runnableexample;

public class Message implements Runnable {
    private String message;

    public Message(String message) {
        this.message = message;
    }

    public void run() {
        while(true) {
            System.out.println(message);
        }
    }
}